# This file is part of sale_confirmed2quotation module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction
from itertools import groupby


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    shipment_number_cache = fields.Char('Shipment number Cache', readonly=True)
    shipment_return_number_cache = fields.Char('Shipment number Cache',
        readonly=True)

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        cls._buttons.update({
            'unprocess': {
                'invisible': Eval('state') != 'processing',
                'icon': 'tryton-back',
                'depends': ['state']
            }
        })

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('shipment_number_cache', None)
        default.setdefault('shipment_return_number_cache', None)
        return super(Sale, cls).copy(records, default=default)

    @classmethod
    def unprocess(cls, sales):
        pool = Pool()
        InvoiceLine = pool.get('account.invoice.line')
        Invoice = pool.get('account.invoice')
        InvoiceTax = pool.get('account.invoice.tax')
        ShipmentOut = pool.get('stock.shipment.out')
        ShipmentOutReturn = pool.get('stock.shipment.out.return')
        Move = pool.get('stock.move')

        # Check if production module is installed
        production_installed = False
        if hasattr(sales[0], 'productions'):
            production_installed = True
            Production = pool.get('production')

        to_write = []
        to_delete_invoices = []
        to_delete_shipments = []
        to_delete_shipments_return = []
        to_delete_productions = []
        shipment_state = cls._get_unprocess_shipment_state()
        for sale in sales:
            if sale.state != 'processing':
                continue

            if sale.invoice_method != 'manual' and any(
                    l.invoice_lines for l in sale.lines):
                to_delete_invoices.extend(
                    (list(set(i for l in sale.lines for i in l.invoice_lines)))
                )

            if not to_write:
                to_write.extend([[sale], {
                    'state': cls._get_unprocess_state()
                }])
            else:
                to_write[0].append(sale)

            if sale.shipment_method == 'order' and (
                    sale.shipments or sale.shipment_returns):
                if len(sale.shipments) == 1:
                    to_write.extend([[sale], {
                        'shipment_number_cache': sale.shipments[0].number
                    }])
                if len(sale.shipment_returns) == 1:
                    to_write.extend([[sale], {'shipment_return_number_cache':
                        sale.shipment_returns[0].number
                    }])
                to_delete_shipments += sale.shipments
                to_delete_shipments_return += sale.shipment_returns

            if production_installed:
                to_delete_productions += sale.productions

        if to_write:
            cls.write(*to_write)

        with Transaction().set_context(_check_access=False), \
                Transaction().set_context(check_origin=False):
            if to_delete_invoices:
                invoices = list(set([l.invoice for l in to_delete_invoices
                    if l.invoice]))
                for line in to_delete_invoices:
                    if not line.party:
                        line.party = line.invoice.party
                    if not line.currency:
                        line.currency = line.invoice.currency
                    # clear invoice relation to avoid other sales reprocessing
                    line.invoice = None
                    line.origin = None
                InvoiceLine.save(to_delete_invoices)

                # reload for get empty origin
                to_delete_invoices = InvoiceLine.browse([
                    l.id for l in to_delete_invoices])
                InvoiceLine.delete(to_delete_invoices)
                if invoices:
                    lines = InvoiceLine.search([
                        ('invoice', 'in', list(map(int, invoices))),
                        ('type', '=', 'line')])
                    if not lines:
                        Invoice.delete(invoices)
                    else:
                        invoice_taxes = [tax for invoice in invoices
                            for tax in invoice.taxes]
                        if invoice_taxes:
                            InvoiceTax.delete(invoice_taxes)
                        for invoice in invoices:
                            invoice.on_change_lines()
                            invoice.save()

            def _get_moves_by_origin(moves):
                _moves = sorted(moves, key=lambda x: str(x.origin))
                moves_origin = []
                for origin, grouped_moves in groupby(_moves,
                        key=lambda x: str(x.origin)):
                    moves_origin.extend([list(grouped_moves),
                        {'origin': origin}])
                return moves_origin

            if to_delete_shipments:
                moves = [move
                    for shipment in to_delete_shipments
                    for move in list(shipment.outgoing_moves) if move.origin]
                moves_origin = _get_moves_by_origin(moves)
                Move.write(moves, {'origin': None})
                ShipmentOut.cancel(to_delete_shipments)
                if shipment_state == 'delete':
                    ShipmentOut.delete(to_delete_shipments)
                elif moves_origin:
                    Move.write(*moves_origin)
            if to_delete_shipments_return:
                moves = [move
                    for shipment in to_delete_shipments
                    for move in list(shipment.incoming_moves) if move.origin]
                moves_origin = _get_moves_by_origin(moves)
                Move.write(moves, {'origin': None})
                ShipmentOutReturn.cancel(to_delete_shipments_return)
                if shipment_state == 'delete':
                    ShipmentOutReturn.delete(to_delete_shipments_return)
                elif moves_origin:
                    Move.write(*moves_origin)
            if production_installed and to_delete_productions:
                Production.cancel(to_delete_productions)
                Production.delete(to_delete_productions)

        # reset states
        cls._process_invoice_shipment_states(sales)

        super().draft(sales)

    @classmethod
    def _get_unprocess_shipment_state(cls):
        return 'delete'

    @classmethod
    def _get_unprocess_state(cls):
        return 'draft'

    def _get_shipment_sale(self, Shipment, key):
        pool = Pool()
        Sale = pool.get('sale.sale')

        types = {
            'stock.shipment.out': ('shipments', 'shipment_number_cache'),
            'stock.shipment.out.return': ('shipment_returns',
                'shipment_return_number_cache')
        }
        _type = types[Shipment.__name__]
        number_cache = getattr(self, _type[1], None)

        if (Sale._get_unprocess_shipment_state() == 'cancelled'
                and self.state == 'confirmed'
                and number_cache):
            shipment = Shipment.search([
                    ('number', '=', number_cache),
                    ('state', '=', 'cancelled')],
                order=[('id', 'DESC')],
                limit=1)
            if shipment:
                Shipment.draft(shipment)
                return Shipment(shipment[0].id)

        shipment = super()._get_shipment_sale(Shipment, key)

        if not getattr(self, _type[0]) and number_cache:
            shipment.number = number_cache
        return shipment

    @classmethod
    def process(cls, sales):
        pool = Pool()
        Move = pool.get('stock.move')

        if cls._get_unprocess_shipment_state() == 'cancelled':
            moves = [
                m for sale in sales if sale.shipment_method == 'order'
                for shipment in sale.shipments if shipment.state == 'cancelled'
                for m in shipment.moves if m.state == 'cancelled'
            ]
            if moves:
                Move.delete(moves)
        super().process(sales)
