# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from itertools import groupby
from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.exceptions import UserError
from trytond.i18n import gettext


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    invoice_cache = fields.Many2One('account.invoice', 'Invoice cache',
        readonly=True, select=True)

    @classmethod
    def unprocess(cls, sales):
        pool = Pool()
        InvoiceLine = pool.get('account.invoice.line')

        to_save = []
        to_delete = []
        invoices = []
        party = None
        for sale in sales:
            if party is None:
                # get party from first sale (does not matter what to use)
                party = sale.party
            if sale.invoices:
                invoices = sale._get_invoices_to_unprocess()
            if invoices:
                assert len(invoices) == 1
                invoice = invoices[0]
                sale.invoice_cache = invoice
                to_save.append(sale)
                to_delete.extend([il for l in sale.lines
                    for il in l.invoice_lines if il.invoice == invoice])

        if to_save:
            cls.save(to_save)
        if to_delete:
            InvoiceLine.write(*[to_delete, {
                'party': party.id,  # set a party to avoid required validation
                'invoice': None,
                'origin': None
            }])
            InvoiceLine.delete(to_delete)
            invoice.update_taxes()
        super().unprocess(sales)

    def create_invoice(self):
        invoice = super().create_invoice()
        if invoice == self.invoice_cache:
            self.invoice_cache = None
            self.save()
        return invoice

    def _get_invoice_sale(self):
        if self.invoice_cache and self.invoice_cache.state == 'draft':
            party = self.invoice_party or self.party
            if self.invoice_cache.party != party:
                self.invoice_cache.party = party
                self.invoice_cache.account_receivable_used = \
                    party.account_receivable
                self.invoice_cache.party_tax_identifier = \
                    party.tax_identifier

            if self.invoice_cache.invoice_address != self.invoice_address:
                self.invoice_cache.invoice_address = self.invoice_address

            return self.invoice_cache
        return super()._get_invoice_sale()

    def _get_invoices_to_unprocess(self):
        return [invoice for invoice in self.invoices
            if invoice.number and invoice.state == 'draft'
        ]


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    @classmethod
    def post(cls, records):
        out_invoices = cls._get_unprocess_invoices_to_check(records)

        super().post(records)

        out_invoices = sorted(out_invoices,
            key=lambda x: (x.company, x.sequence))
        for key, grouped_invoices in groupby(out_invoices,
                key=lambda x: (x.company, x.sequence)):
            company, sequence = key
            invoices = cls.search([
                    ('company', '=', company.id),
                    ('type', '=', 'out'),
                    ('id', 'not in', list(map(int, grouped_invoices))),
                    ('number', '!=', None),
                    ('sequence', '=', sequence),
                    ('state', '=', 'draft'),
                ], limit=1)
            if invoices:
                raise UserError(gettext('sale_processing2confirmed.'
                    'msg_invoice_draft_numbered',
                    invoice=invoices[0].rec_name))

    @classmethod
    def _get_unprocess_invoices_to_check(cls, records):
        return [r for r in records if r.type == 'out'
            and r.state not in ('posted', 'paid')]
